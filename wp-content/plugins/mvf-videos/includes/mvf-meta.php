<?php

	function mvf_add_custom_meta() {
		$screens = [ 'mvf-video', 'mvf_cpt' ];
		foreach ( $screens as $screen ) {
			add_meta_box(
				'mvf_box_id', __('Video Details', 'mvf'), 'display_mvf_video_meta_box', $screen 
			);
		}
	}
	add_action( 'add_meta_boxes', 'mvf_add_custom_meta' );
	
	function display_mvf_video_meta_box( $post ) {
		$video_subtitle = esc_html( get_post_meta( $post->ID, '_mvf_video_subtitle', true ) );
		$video_description = esc_html( get_post_meta( $post->ID, '_mvf_video_description', true ) );
		$video_provider = get_post_meta( $post->ID, '_mvf_video_provider', true );
		$video_id = esc_html( get_post_meta( $post->ID, '_mvf_video_id', true ) );
		$video_border = esc_html( get_post_meta( $post->ID, '_mvf_video_border', true ) );
?>
	<table>
		<tr>
			<td style="width: 100%"><?php _e('Subtitle', 'mvf'); ?></td>
			<td><input type="text" size="80" name="mvf_video_subtitle" id="mvf_video_subtitle" value="<?php echo $video_subtitle; ?>" /></td>
		</tr>
		<tr>
			<td style="width: 100%"><?php _e('Description', 'mvf'); ?></td>
			<td><textarea name="mvf_video_description" id="mvf_video_description" rows="4" cols="75"><?php echo $video_description; ?></textarea></td>
		</tr>
		<tr>
			<td style="width: 150px"><?php _e('Video Provider', 'mvf'); ?></td>
			<td>
				<select style="width: 100px" name="mvf_video_provider" id="mvf_video_provider" class="postbox">
					<option value="YouTube" <?php selected( $video_provider, 'YouTube' ); ?>>YouTube</option>
					<option value="Vimeo" <?php selected( $video_provider, 'Vimeo' ); ?>>Vimeo</option>
					<option value="Dailymotion" <?php selected( $video_provider, 'Dailymotion' ); ?>>Dailymotion</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width: 100%"><?php _e('Video ID', 'mvf'); ?></td>
			<td><input type="text" size="80" name="mvf_video_id" id="mvf_video_id" value="<?php echo $video_id; ?>" /></td>
		</tr>
	</table>
<?php
	}
	
	function mvf_save_postdata( $post_id ) {
		if ( array_key_exists( 'mvf_video_subtitle', $_POST ) ) {
			update_post_meta( $post_id, '_mvf_video_subtitle', $_POST['mvf_video_subtitle'] );
		}
		if ( array_key_exists( 'mvf_video_description', $_POST ) ) {
			update_post_meta( $post_id, '_mvf_video_description', $_POST['mvf_video_description'] );
		}
		if ( array_key_exists( 'mvf_video_provider', $_POST ) ) {
			update_post_meta( $post_id, '_mvf_video_provider', $_POST['mvf_video_provider'] );
		}
		if ( array_key_exists( 'mvf_video_id', $_POST ) ) {
			update_post_meta( $post_id, '_mvf_video_id', $_POST['mvf_video_id'] );
		}
	}
	add_action( 'save_post', 'mvf_save_postdata' );