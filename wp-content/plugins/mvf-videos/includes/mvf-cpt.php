<?php

	function create_mvf_video() {
		register_post_type( 'mvf-video',
			array(
				'labels' => array(
					'name'					=> _x( 'Videos', 'mvf' ),
					'singular_name' 		=> _x( 'Video', 'mvf' ),
					'add_new'				=> _x( 'Add New', 'mvf' ),
					'add_new_item'			=> _x( 'Add New Video', 'mvf' ),
					'edit'					=> _x( 'Edit', 'mvf' ),
					'edit_item'				=> _x( 'Edit Video', 'mvf' ),
					'new_item'				=> _x( 'New Video', 'mvf' ),
					'view'					=> _x( 'View', 'mvf' ),
					'view_item'				=> _x( 'View Video', 'mvf' ),
					'search_items'			=> _x( 'Search Videos', 'mvf' ),
					'not_found'				=> _x( 'No Videos found', 'mvf' ),
					'not_found_in_trash'	=> _x( 'No Videos found in Trash', 'mvf' ),
					'parent'				=> _x( 'Parent Video', 'mvf' )
				),
	 
				'public'					=> true,
				'menu_position'				=> 15,
				'supports'					=> array( 'title' ),
				'has_archive'				=> false
			)
		);
	}
	add_action( 'init', 'create_mvf_video' );