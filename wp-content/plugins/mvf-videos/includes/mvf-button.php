<?php

	function mvf_video_button($buttons) {
		array_push($buttons, 'mvf_video');
		return $buttons;
	}
	add_filter('mce_buttons', 'mvf_video_button');
	
	function mvf_video_js($plugin_array) {
		$plugin_array['mvf_video'] = plugins_url('/assets/mvf-button.js',__FILE__);
		return $plugin_array;
	}
	add_filter('mce_external_plugins', 'mvf_video_js');
	
	function mvf_video_css() {
		wp_enqueue_style('tinymce-mvf_video-button', plugins_url('/assets/mvf-icon.css', __FILE__));
	}
	add_action( 'admin_enqueue_scripts', 'mvf_video_css' );
	add_action( 'wp_enqueue_scripts', 'mvf_video_css' );
