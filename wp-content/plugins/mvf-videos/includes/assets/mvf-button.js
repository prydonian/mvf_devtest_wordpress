(function() {

	tinymce.PluginManager.add('mvf_video', function(editor) {
		editor.addButton('mvf_video', {
			classes: 'mvf_video',
			icon: false,
			onclick: function() {
				editor.windowManager.open({
					body: [{
						type: 'textbox',
						name: 'postID',
						label: 'Post ID',
						value: '',
						classes: 'mvf_post_id',
					}, {
						type: 'colorpicker',
						name: 'borderColor',
						label: 'Border Colour',
						value: '',
						classes: 'mvf_border_color',
					}],
					onsubmit: function(e) {
						editor.insertContent( '[mvf_video id="' + e.data.postID + '" border_color="' + e.data.borderColor + '"]');
					}
				})
			}
		});
	});
})();
