<?php

	function mvf_video_shortcode( $atts = array(), $content = null, $tag = '' ) {
	$mvf_atts = shortcode_atts(
		array(
			'id'			=> '',
			'border_color'	=> ''
		), $atts, $tag
	);
	$mvf_video_post = get_post($mvf_atts['id']);
	$video_subtitle = esc_html( get_post_meta( $mvf_atts['id'], '_mvf_video_subtitle', true ) );
	$video_description = esc_html( get_post_meta( $mvf_atts['id'], '_mvf_video_description', true ) );
	$video_provider = get_post_meta( $mvf_atts['id'], '_mvf_video_provider', true );
	$video_id = esc_html( get_post_meta( $mvf_atts['id'], '_mvf_video_id', true ) );
	$video_border = esc_html( get_post_meta( $mvf_atts['id'], '_mvf_video_border', true ) );
	$mvf_output = '<div class="mvf-video-grid">';
		$mvf_output .= '<div class="mvf-video-container">';
			$mvf_output .= '<div class="mvf-responsive-video" style="border-color: '.$mvf_atts['border_color'].'">';
			switch ($video_provider) {
				case "YouTube":
					$mvf_output .= '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$video_id.'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
					break;
				case "Vimeo":
					$mvf_output .= '<iframe width="560" height="315" src="https://player.vimeo.com/video/'.$video_id.'" title="Vimeo video player" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
					break;
				case "Dailymotion":
					$mvf_output .= '<iframe width="560" height="315" src="https://www.dailymotion.com/embed/video/'.$video_id.'" title="Dailymotion video player" frameborder="0" allow="accelerometer; autoplay;" allowfullscreen></iframe>';
					break;
			}
			$mvf_output .= '</div>';
		$mvf_output .= '</div>';
		$mvf_output .= '<div class="mvf-video-content">';
			$mvf_output .= '<h4>'.$mvf_video_post->post_title.'</h4>';
			$mvf_output .= '<h5>'.$video_subtitle.'</h5>';
			$mvf_output .= '<p>'.$video_description.'</p>';
		$mvf_output .= '</div>';
	$mvf_output .= '</div>';
	return $mvf_output;
}
add_shortcode( 'mvf_video', 'mvf_video_shortcode' );