<?php
	/*
	Plugin Name: MVF Videos
	Plugin URI: https://www.mvfglobal.com/
	Description: A plugin that creates a Videos custom post type with options for Title, Subtitle, Description, Video ID, and Type
	Version: 1
	Author: Mark Duwe
	Author URI: https://prydonian.digital
	Text Domain: mvf
	*/
	
	require_once __DIR__ . '/includes/mvf-cpt.php';
	require_once __DIR__ . '/includes/mvf-meta.php';
	require_once __DIR__ . '/includes/mvf-shortcode.php';
	require_once __DIR__ . '/includes/mvf-button.php';
	
	function mvf_css() {
		$plugin_url = plugin_dir_url( __FILE__ );
		wp_enqueue_style( 'mvf_video_css', $plugin_url . 'includes/assets/mvf-style.css' );
	}
	add_action( 'wp_enqueue_scripts', 'mvf_css' );
