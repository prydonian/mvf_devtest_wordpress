## MVF Dev test

This is my solution for the dev test.

It's a plugin that adds a Custom Post Type called "Videos" that has custom meta "Subtitle", "Description", "Video Provider" and "Video ID".

It outputs a video and text description when added via a shortcode.

The video and the layout are both responsive.

The shortcode can be added by clicking the TinyMCE button in the editor. This pops up a modal where you can add the post ID and the colour for the video border.

